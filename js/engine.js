
$(function() {



 

// Бургер меню
$(".burger-menu").click(function(){
  $('.page-menu').fadeToggle(200);
  $('body').addClass('overflow-hidden');
});



// МОБИЛЬНОЕ меню

$('.page-menu__wrap > ul > li.dropdown > span').click(function(){
$(this).parents('ul').toggleClass('opened');
$(this).parents('li').toggleClass('dropdown-opened');

});

$('.page-menu .back').click(function(){
$(this).parents('.dropdown-opened').toggleClass('dropdown-opened').parents('ul').toggleClass('opened');
});


// Закрываем мобильное меню
$('.close-mmenu').click(function() {
  $('body').removeClass('overflow-hidden');
  $('.page-menu').fadeToggle(200);
})
$(document).mouseup(function (e){ // событие клика по веб-документу
  var div = $(".page-menu"); // тут указываем ID элемента
  if (!div.is(e.target) // если клик был не по нашему блоку
      && div.has(e.target).length === 0) { // и не по его дочерним элементам
    div.hide(); // скрываем его
  if ($('body').hasClass('overflow-hidden')) {
    $('body').removeClass('overflow-hidden')
  }
    
  }
});


// КОНЕЦ МОБИЛЬНОГО МЕНЮ






// Скролл по номерах отеля
$(".room-links").on("click","a", function (event) {
    //отменяем стандартную обработку нажатия по ссылке
    event.preventDefault();

    //забираем идентификатор бока с атрибута href
    var id  = $(this).attr('href'),

    //узнаем высоту от начала страницы до блока на который ссылается якорь
      top = $(id).offset().top;
    
    //анимируем переход на расстояние - top за 1500 мс
    $('body,html').animate({scrollTop: top - 90}, 1500);
  });




// FansyBox
 $('.fancybox').fancybox({});


// Сллайдер на главной
let homeSlider = $('.home-slider').slick({
  dots: false,
  arrows: false,
  speed: 300,
  slidesToShow: 1,
  slidesToScroll: 1,
  fade: true,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        dots: true,
      }
    },
  ]
});
$('.cm-button-prev').click(function(){
    $(homeSlider).slick("slickPrev")
});
$('.cm-button-next').click(function(){
    $(homeSlider).slick("slickNext")
});
$('.cm-items li a').click(function(e) {
   e.preventDefault();
   var slideno = $(this).attr('href');
    console.log(slideno)
   $('.home-slider').slick('slickGoTo', slideno - 1);
 });



// Слайдер Галереи
$('.gallery-slider').slick({
  dots: false,
  arrows: true,
  speed: 300,
  slidesToShow: 1,
  slidesToScroll: 1,
  fade: true
  // autoplay: true,
  // autoplaySpeed: 5000,
});




// Стилизация селектов
$('select').styler();


// Календарь
$(function () {
    $('.datetimepicker input').datetimepicker({
        locale: 'ru',
        format: 'DD.MM.YYYY'

    });
});

// Подсказка при наведении
$('.question-hover').hover(function() {
  $(this).children('.question-hover__text').stop(true,true).fadeToggle()
})



//Табы
$('.tabs-nav a').click(function() {
    var _id = $(this).attr('href');
    var _targetElement = $('.tabs-wrapper').find('#' + _id);
    $('.tabs-nav a').removeClass('active');
    $(this).addClass('active');
    $('.tabs-item').removeClass('active');
    _targetElement.addClass('active');
    return false;

});


$('.tabs-mobile__btn').click(function() {
    var _targetElementParent = $(this).parent('.tabs-item');
    _targetElementParent.toggleClass('active-m');
    _targetElementParent.find('.tabs-item__body').slideToggle(600);
    $('.tabs-mobile__btn').parent('.tabs-item').removeClass('active');
    return false;
});







})


